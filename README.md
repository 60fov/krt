# KRT
/ˈkɹɪtɪk/, *pronounced like the **crit** in **crit**ical*

KRT is a Korean Typing App I developed to help me learn the language and how to type it.

https://60fov.gitlab.io/krt/

## How-to
type what you see in the prompt, then do it again.

## Key Binds
shift + enter to skip.
ctrl + enter to replay audio. (also the cmd key if on mac)

## Use Recommendations
For most effectiveness, first, familiarize yourself with the Korean alphabet, Hangul, focusing on pronunciation and the [block system](https://en.wikipedia.org/wiki/Hangul#Morpho-syllabic_blocks). I prefer [this video](https://www.youtube.com/watch?v=TVqJbiSLw-E) and it's [sequel](https://www.youtube.com/watch?v=S31_x00UoQ0) for pronunciation. As for the block system, any resource will probably work, it's fairly simple. Hangul's difficulty is comparable to English's alphabet. The most difficult things are mixed sounds (ㄱ is a mix of k and g), sound variants (ㅂ vs ㅃ) and there being sounds in Korean that don't exist in English (ㅡ). 

Now that you've decided to skip learning hangul, hop into KRT and start in jamo-mode and become familiar with where they are on the keyboard.

Once you're familiar with where everything is, switch to phrase-mode and learn a handful of basic phrases and their pronunciations. As you type out the phrase do practice pronouncing it yourself for maximum effectiveness.

## Toolchain + Libraries
[vitejs](https://vitejs.dev/) + [svelte](https://svelte.dev/) + some yoink'd [repo](https://github.com/elicir/QWERTY-to-Hangul), 감사합니다!

## Soon™
- [ ]  improved input focus UX
- [ ]  non-coincidental mobile support
- [ ]  user performance metrics
- [ ]  actual TTS
- [ ]  alternate keyboard layouts 
- [ ]  better theme support
- [ ]  more Accurate IME simulation

## Contributing

issues and PRs welcome, but no promises.

just make it make sense.


## Resources
non-exhaustive list of resources I use to learn korean and develop this app.

[Hangul Wikipedia](https://en.wikipedia.org/wiki/Hangul)

[Constant and Vowels Table](https://en.wikipedia.org/wiki/Hangul_consonant_and_vowel_tables)

[omniglot](https://www.omniglot.com/writing/korean.htm)

[Romanization Tool](http://roman.cs.pusan.ac.kr/input_eng.aspx?)

[Free TTS](https://freetts.com/Home/KoreanTTS)

[Papago](https://papago.naver.com/)


