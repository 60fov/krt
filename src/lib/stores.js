import { writable } from "svelte/store";

export const store_settings = writable({
        "latin": (localStorage.getItem("latin") || 'true') === 'true',
        "hangul": (localStorage.getItem("hangul") || 'true') === 'true',
        "shift": (localStorage.getItem("shift") || 'true') === 'true',
        "shiftswap": (localStorage.getItem("shiftswap") || 'false') === 'true',
        "autoret": (localStorage.getItem("autoret") || 'false') === 'true',
        "roman": (localStorage.getItem("roman") || 'false') === 'true',
        "theme": localStorage.getItem("theme") || 'system',
        "mode": localStorage.getItem("mode") || 'jamo',
    }
)

export const store_shift_key = writable(false);