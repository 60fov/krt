import anjeuseyo from "../assets/audio/anjeuseyo.mp3"
import annyonghaseyo from "../assets/audio/annyonghaseyo.mp3"
import annyonghi_gyeseyo from "../assets/audio/annyonghi_gyeseyo.mp3"
import bangapsseumnida from "../assets/audio/bangapsseumnida.mp3"
import chonchonhi_malhaejuseyo from "../assets/audio/chonchonhi_malhaejuseyo.mp3"
import dangsin_ireumi_muosimnikka from "../assets/audio/dangsin_ireumi_muosimnikka.mp3"
import dasi_malhaejuseyo from "../assets/audio/dasi_malhaejuseyo.mp3"
import deurooseyo from "../assets/audio/deurooseyo.mp3"
import gago_sipsseumnida from "../assets/audio/gago_sipsseumnida.mp3"
import gamsahamnida from "../assets/audio/gamsahamnida.mp3"
import gapssida from "../assets/audio/gapssida.mp3"
import haljjuramnikka from "../assets/audio/haljjuramnikka.mp3"
import hanguktton from "../assets/audio/hanguktton.mp3"
import igosi_muosimnikka from "../assets/audio/igosi_muosimnikka.mp3"
import jal_jinaeyo from "../assets/audio/jal_jinaeyo.mp3"
import jamsimannyo from "../assets/audio/jamsimannyo.mp3"
import joneun_baegopeumnida from "../assets/audio/joneun_baegopeumnida.mp3"
import joneun_pigonhamnida from "../assets/audio/joneun_pigonhamnida.mp3"
import miguktton from "../assets/audio/miguktton.mp3"
import odigamnikka from "../assets/audio/odigamnikka.mp3"
import olmaimnikka from "../assets/audio/olmaimnikka.mp3"
import oraeganmanimnada from "../assets/audio/oraeganmanimnada.mp3"
import ottokejinaessoyo from "../assets/audio/ottokejinaessoyo.mp3"
import saranghae from "../assets/audio/saranghae.mp3"
import silryehamnida from "../assets/audio/silryehamnida.mp3"
import ttobopssida from "../assets/audio/ttobopssida.mp3"

import txt from "../assets/kr.txt?raw"

export const LUT_KEY_HANGUL = {
    "Q": "ㅃ",
    "W": "ㅉ",
    "E": "ㄸ",
    "R": "ㄲ",
    "T": "ㅆ",
    "O": "ㅒ",
    "P": "ㅖ",
    
    "q": "ㅂ",
    "w": "ㅈ",
    "e": "ㄷ",
    "r": "ㄱ",
    "t": "ㅅ",
    "y": "ㅛ",
    "u": "ㅕ",
    "i": "ㅑ",
    "o": "ㅐ",
    "p": "ㅔ",
    
    "a": "ㅁ",
    "s": "ㄴ",
    "d": "ㅇ",
    "f": "ㄹ",
    "g": "ㅎ",
    "h": "ㅗ",
    "j": "ㅓ",
    "k": "ㅏ",
    "l": "ㅣ",
    
    "z": "ㅋ",
    "x": "ㅌ",
    "c": "ㅊ",
    "v": "ㅍ",
    "b": "ㅠ",
    "n": "ㅜ",
    "m": "ㅡ"
};

export const JAMO = [
    "ㅃ","ㅉ","ㄸ","ㄲ","ㅆ","ㅒ","ㅖ",
    "ㅂ","ㅈ","ㄷ","ㄱ","ㅅ", "ㅛ","ㅕ","ㅑ","ㅐ","ㅔ",
    "ㅁ","ㄴ","ㅇ","ㄹ","ㅎ","ㅗ","ㅓ","ㅏ","ㅣ",
    "ㅋ","ㅌ","ㅊ","ㅍ","ㅠ","ㅜ","ㅡ"
];

export const PHRASE = [
    { ko: "안녕하세요", en: "Hello!", roman: "annyonghaseyo.", audio: annyonghaseyo },
    { ko: "잠시만요", en: "Just a moment.", roman: "jamsimannyo.", audio: jamsimannyo },
    { ko: "잘 지내요", en: "I am good.", roman: "jal jinaeyo.", audio: jal_jinaeyo },
    { ko: "안녕히 계세요", en: "Goodbye.", roman: "annyonghi gyeseyo.", audio: annyonghi_gyeseyo },
    { ko: "사랑해", en: "I love you.", roman: "saranghae.", audio: saranghae },
    { ko: "갑시다", en: "Let's go.", roman: "gapssida.", audio: gapssida },
    { ko: "반갑습니다", en: "Pleased to meet you.", roman: "bangapsseumnida.", audio: bangapsseumnida },
    { ko: "오래간만입나다", en: "Long time no see", roman: "oraeganmanimnada.", audio: oraeganmanimnada },
    { ko: "한국돈", en: "Korean money", roman: "hanguktton.", audio: hanguktton },
    { ko: "미국돈", en: "American money", roman: "miguktton.", audio: miguktton },
    { ko: "어떻게지냈어요", en: "How have you been?", roman: "ottokejinaessoyo.", audio: ottokejinaessoyo },
    { ko: "어디갑니까?", en: "Where are you going?", roman: "odigamnikka?", audio: odigamnikka },
    { ko: "들어오세요", en: "Please come in.", roman: "deurooseyo.", audio: deurooseyo },
    { ko: "앉으세요", en: "Please sit down.", roman: "anjeuseyo.", audio: anjeuseyo },
    { ko: "얼마입니까?", en: "How much is it?", roman: "olmaimnikka?", audio: olmaimnikka },
    { ko: "감사합니다", en: "Thank You.", roman: "gamsahamnida.", audio: gamsahamnida },
    { ko: "당신 이름이 무엇입니까?", en: "What's your name?", roman: "dangsin ireumi muosimnikka?", audio: dangsin_ireumi_muosimnikka },
    { ko: "이것이 무엇입니까?", en: "What is this?", roman: "igosi muosimnikka?", audio: igosi_muosimnikka },
    { ko: "다시 말해주세요", en: "Please say it again.", roman: "dasi malhaejuseyo.", audio: dasi_malhaejuseyo },
    { ko: "천천히 말해주세요", en: "Please speak slowly.", roman: "chonchonhi malhaejuseyo.", audio: chonchonhi_malhaejuseyo },
    { ko: "할줄압니까?", en: "Can you speak English?", roman: "haljjuramnikka?", audio: haljjuramnikka },
    { ko: "실례합니다", en: "Excuse me.", roman: "silryehamnida.", audio: silryehamnida },
    { ko: "또봅시다", en: "See you again.", roman: "ttobopssida.", audio: ttobopssida },
    { ko: "가고 싶습니다", en: "I want to go.", roman: "gago sipsseumnida.", audio: gago_sipsseumnida },
    { ko: "저는 피곤합니다", en: "I'm tired.", roman: "joneun pigonhamnida.", audio: joneun_pigonhamnida },
    { ko: "저는 배고픕니다", en: "I'm hungry.", roman: "joneun baegopeumnida.", audio: joneun_baegopeumnida },
];

let dyn_phrases = [];

loadPhrases();

export function loadPhrases() {
    dyn_phrases = txt
    .trim()
    .split("\n")
    .filter(line => line != "")
    .map(line => {
        let ab = line.trim().split("|");
        return { ko: ab[0].trim(), en: ab[1].trim() };
    });
}

export function randomJamo() {
    return JAMO[Math.floor(Math.random()*JAMO.length)];
}

export function randomWord(){
}

export function randomPhrase() {
    return dyn_phrases[Math.floor(Math.random()*dyn_phrases.length)];
    // return PHRASE[Math.floor(Math.random()*PHRASE.length)];
}